//sender - subsribed to acceleration, braking, steering, indicators, neutralize commands. create a can massage for the Idan drive by wire system 
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/PoseStamped.h>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

static const int SYNC_FRAMES = 50;

typedef message_filters::sync_policies::ApproximateTime<geometry_msgs::TwistStamped, geometry_msgs::PoseStamped>//ApproximateTime
    TwistPoseSync;

class SyncPoseVel
{
public:
  SyncPoseVel();
  ~SyncPoseVel();
private:
ros::Subscriber vel_sub,pose_sub;
ros::Publisher vel_pub,pose_pub;
  message_filters::Subscriber<geometry_msgs::TwistStamped> *sub_vel_;
  message_filters::Subscriber<geometry_msgs::PoseStamped> *sub_pose_;
    message_filters::Synchronizer<TwistPoseSync> *sync_tp_;

  void TwistPoseCallback(const geometry_msgs::TwistStampedConstPtr &twist_msg,
                      const geometry_msgs::PoseStampedConstPtr &pose_msg);

  void poseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);
};

SyncPoseVel::~SyncPoseVel()
{
}
SyncPoseVel::SyncPoseVel()
{
ros::NodeHandle n;

  // vel_sub = n.subscribe("gnss_velocity_unsynced",1000,velCallback);
   //pose_sub = n.subscribe("gnss_pose_unsynced",1000,&SyncPoseVel::poseCallback, this);
  vel_pub = n.advertise<geometry_msgs::TwistStamped>("gnss_velocity", 10);
  pose_pub = n.advertise<geometry_msgs::PoseStamped>("gnss_pose", 10);





  sub_pose_ = new message_filters::Subscriber<geometry_msgs::PoseStamped>(n, "gnss_pose_unsynced", 50);
  sub_vel_ = new message_filters::Subscriber<geometry_msgs::TwistStamped>(n, "gnss_velocity_unsynced", 50);
  sync_tp_ = new message_filters::Synchronizer<TwistPoseSync>(TwistPoseSync(SYNC_FRAMES), *sub_vel_, *sub_pose_);
  sync_tp_->registerCallback(boost::bind(&SyncPoseVel::TwistPoseCallback, this, _1, _2));

}

void SyncPoseVel::TwistPoseCallback(const geometry_msgs::TwistStampedConstPtr &twist_msg,
                                      const geometry_msgs::PoseStampedConstPtr &pose_msg) //const
{
  geometry_msgs::TwistStamped ts = *twist_msg;
  ts.header = pose_msg->header;//the twist message is now with the same time stamp
  vel_pub.publish(ts);
  pose_pub.publish(*pose_msg);
  
}

// geometry_msgs::TwistStamped ts;
// bool initialized = false;
// void velCallback(const geometry_msgs::TwistStamped::ConstPtr& msg)
// {
//   if (!initialized)
//     initialized = true;
//   //ROS_WARN_STREAM("I heard:"<< msg->twist.linear);
//   ts = *msg;
// }
// void SyncPoseVel::poseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
// {
//   ROS_WARN_STREAM("I heard:"<< msg->pose.position);

//   // ts.header = msg->header;//the twist message is now with the same time stamp

//   // if (initialized)
//   // {
//   // vel_pub.publish(ts);
//   // pose_pub.publish(*msg);
//   // }
// }

int main (int argc, char *argv[])

{
  ros::init(argc, argv, "sync_pose_vel");
  SyncPoseVel SPV;
  ros::spin();
  return 0;
}





