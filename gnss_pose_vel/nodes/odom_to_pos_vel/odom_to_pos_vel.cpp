//sender - subsribed to acceleration, braking, steering, indicators, neutralize commands. create a can massage for the Idan drive by wire system 
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/TwistStamped.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Vector3.h>
//#include <geometry_msgs/AccelStamped.h> 

// void publishAccelStamped(ros::Publisher accel_pub,ros::Time current_time,double angular_x,double angular_y,double angular_z, double linear_x, double linear_y, double linear_z)
// {
//   //geometry_msgs::Accel accel;
//   //twist.
//   geometry_msgs::AccelStamped as;
//   as.header.stamp = current_time;
//   as.accel.angular.x = angular_x;
//   as.accel.angular.y = angular_y;
//   as.accel.angular.z = angular_z;
//   as.accel.linear.x = linear_x;
//   as.accel.linear.y = linear_y;
//   as.accel.linear.z = linear_z;
//   accel_pub.publish(as);
// }

class Odom2PosVel
{
public:
  Odom2PosVel();
  ~Odom2PosVel();
private:
  void publishTF(geometry_msgs::Pose pose,ros::Time stamp);//
  void odomCallback(const nav_msgs::Odometry::ConstPtr& msg);
  void imuCallback(const sensor_msgs::Imu::ConstPtr& msg);
  ros::Publisher vel_pub_;//,acc_pub_;
  ros::Subscriber odom_sub_,imu_sub_;
  tf::TransformBroadcaster br_;
  double last_t = 0;
  //double acc_x_ = 0,acc_y_ = 0,acc_z_ = 0;
  geometry_msgs::Vector3 angular_vel;

};
void Odom2PosVel::imuCallback(const sensor_msgs::Imu::ConstPtr& msg)
{
  angular_vel.x = msg->angular_velocity.x;
  angular_vel.y = msg->angular_velocity.y;
  angular_vel.z = msg->angular_velocity.z;
}

Odom2PosVel::Odom2PosVel()
{
  ros::NodeHandle n;
  odom_sub_ = n.subscribe("odom",1000,&Odom2PosVel::odomCallback,this);
  imu_sub_ = n.subscribe("/imu",1000,&Odom2PosVel::imuCallback,this);
  vel_pub_ = n.advertise<geometry_msgs::TwistStamped>("current_velocity", 10);
  angular_vel.x = 0;
  angular_vel.y = 0;
  angular_vel.z = 0;
  ros::spin();
}
Odom2PosVel::~Odom2PosVel(){}
 

void Odom2PosVel::publishTF(geometry_msgs::Pose pose,ros::Time stamp)//
{
  tf::Transform transform;
  double roll, pitch, yaw;
  //transform.setOrigin(tf::Vector3(geo_.y(), geo_.x(), geo_.z()));
  //transform.setOrigin(tf::Vector3(pose.position.y, pose.position.x, pose.position.z));
  transform.setOrigin(tf::Vector3(pose.position.x, pose.position.y, pose.position.z));
  // tf::Quaternion quaternion;
  // quaternion.setRPY(roll_, pitch_, yaw_);
  tf::Quaternion quat_tf;
  quaternionMsgToTF(pose.orientation , quat_tf);
   tf::Matrix3x3(quat_tf).getRPY(roll, pitch, yaw);
  //tf::Matrix3x3(quat_tf).getRPY(roll, yaw , pitch);
  // yaw += 3.141;
  // yaw = -yaw;
  yaw += 3.141/2;
  quat_tf.setRPY(roll, pitch, yaw);
  transform.setRotation(quat_tf);
  br_.sendTransform(tf::StampedTransform(transform, stamp , "gnss_frame", "gps"));
}




void Odom2PosVel::odomCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  //ROS_WARN_STREAM("I heard:"<< msg->twist.twist);
  ros::Time current_time = ros::Time::now();
  geometry_msgs::TwistStamped ts;
  
  ts.header = msg->header;
  double send_time = current_time.toSec();//msg->header.stamp.toSec();
  //double dt = send_time - last_t;
  // double dt1 = 1./15;
  // double ddt = 0.001;
  // if(dt > dt1 + ddt || dt < dt1- ddt)
  //   ROS_WARN_STREAM("send_time: "<<send_time<<" current_time: "<<current_time.toSec()<<" dt: "<<dt);
  //last_t = send_time;
  ts.header.stamp = current_time;//ros::Time::now();
  ts.twist = msg->twist.twist;
  ts.twist.angular = angular_vel;
  vel_pub_.publish(ts);

  publishTF(msg->pose.pose,current_time );//msg->header.stamp  
  
  
}

int main (int argc, char *argv[])

{
  ros::init(argc, argv, "odom_to_pos_vel");
  Odom2PosVel OPV;
  return 0;
}





