//sender - subsribed to acceleration, braking, steering, indicators, neutralize commands. create a can massage for the Idan drive by wire system 
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/TwistStamped.h>

ros::Publisher vel_pub;
void odomCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  //ROS_WARN_STREAM("I heard:"<< msg->twist.twist);
  geometry_msgs::TwistStamped ts;
  ts.header = msg->header;
  ts.header.stamp = ros::Time::now();
  ts.twist = msg->twist.twist;
  vel_pub.publish(ts);
  
}

int main (int argc, char *argv[])

{
  ros::init(argc, argv, "republish_vel");
  ros::NodeHandle n;

  ros::Subscriber odom_sub = n.subscribe("odom",1000,odomCallback);
  vel_pub = n.advertise<geometry_msgs::TwistStamped>("gnss_velocity_unsynced", 10);
  ros::spin();
  return 0;
}





