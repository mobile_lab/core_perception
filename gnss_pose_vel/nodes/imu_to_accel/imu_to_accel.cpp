//sender - subsribed to acceleration, braking, steering, indicators, neutralize commands. create a can massage for the Idan drive by wire system 
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/AccelStamped.h> 
#include <geometry_msgs/TwistStamped.h>

#include <iostream>

void publishAccelStamped(ros::Publisher accel_pub,ros::Time current_time,double angular_x,double angular_y,double angular_z, double linear_x, double linear_y, double linear_z)
{
  //geometry_msgs::Accel accel;
  //twist.
  geometry_msgs::AccelStamped as;
  as.header.stamp = current_time;
  as.accel.angular.x = angular_x;
  as.accel.angular.y = angular_y;
  as.accel.angular.z = angular_z;
  as.accel.linear.x = linear_x;
  as.accel.linear.y = linear_y;
  as.accel.linear.z = linear_z;
  accel_pub.publish(as);
}

class Imu2Accel
{
public:
  Imu2Accel();
  ~Imu2Accel();
private:
  void imuCallback(const sensor_msgs::Imu::ConstPtr& msg);
  void twistCallback(const geometry_msgs::TwistStamped::ConstPtr& msg);
  ros::Publisher acc_pub_;
  ros::Subscriber imu_sub_,twist_sub_;
  double acc_x_ = 0,acc_y_ = 0,acc_z_ = 0;
  double acc_x_last_ = 0,acc_y_last_ = 0,acc_z_last_ = 0;
  int index_ = 0;
  static const int moving_average_num_ = 20;
  double acc_x_vec_[moving_average_num_] = {0};
  double acc_y_vec_[moving_average_num_] = {0};
  double acc_z_vec_[moving_average_num_] = {0};
};

Imu2Accel::Imu2Accel()
{
  ros::NodeHandle n;
  twist_sub_ = n.subscribe("/current_velocity",1000,&Imu2Accel::twistCallback,this);
  imu_sub_ = n.subscribe("/imu",1000,&Imu2Accel::imuCallback,this);
  acc_pub_ = n.advertise<geometry_msgs::AccelStamped>("current_acceleration", 10);
  ros::spin();
}
Imu2Accel::~Imu2Accel(){}
 


void Imu2Accel::twistCallback(const geometry_msgs::TwistStamped::ConstPtr& msg)
{
  ros::Time current_time = msg->header.stamp;
  publishAccelStamped(acc_pub_,current_time,0,0,0,acc_x_,acc_y_,acc_z_ );
  
}

void Imu2Accel::imuCallback(const sensor_msgs::Imu::ConstPtr& msg)
{
  // ros::Time current_time = msg->header.stamp;//ros::Time::now();
   
  acc_x_vec_[index_] = msg->linear_acceleration.x;
  acc_y_vec_[index_] = msg->linear_acceleration.y;
  acc_z_vec_[index_] = msg->linear_acceleration.z;
  index_++;
  if (index_ >= moving_average_num_)
    index_ = 0;

  
  for(int i = 0 ; i<moving_average_num_; i++)
    ROS_WARN_STREAM(acc_x_vec_[i]);
  // acc_x_ += (acc_x_vec_[index_] - acc_x_vec_[(index_+1)%moving_average_num_ ])/moving_average_num_;
  // acc_y_ += (acc_y_vec_[index_] - acc_y_vec_[(index_+1)%moving_average_num_ ])/moving_average_num_;
  // acc_z_ += (acc_z_vec_[index_] - acc_z_vec_[(index_+1)%moving_average_num_ ])/moving_average_num_;
  acc_x_ = 0;
  for(int i = 0 ; i<moving_average_num_; i++)
    acc_x_+= acc_x_vec_[i];
  acc_x_ /= moving_average_num_;
  acc_y_ = 0;
  for(int i = 0 ; i<moving_average_num_; i++)
    acc_y_+= acc_y_vec_[i];
  acc_y_ /= moving_average_num_;
  acc_z_ = 0;
  for(int i = 0 ; i<moving_average_num_; i++)
    acc_z_+= acc_z_vec_[i];
  acc_z_ /= moving_average_num_;


  ROS_WARN_STREAM("index: "<<index_<<" last: "<<(index_+1)%moving_average_num_<<" acc_x:"<<acc_x_);
  // publishAccelStamped(acc_pub_,current_time,0,0,0, msg->linear_acceleration.x, msg->linear_acceleration.y, msg->linear_acceleration.z);
  // acc_x_ = msg->linear_acceleration.x;
  // acc_y_ = msg->linear_acceleration.y;
  // acc_z_ = msg->linear_acceleration.z;
}

int main (int argc, char *argv[])

{
  ros::init(argc, argv, "imu_to_accel");
  Imu2Accel I2A;
  return 0;
}








