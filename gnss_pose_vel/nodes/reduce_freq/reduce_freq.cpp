//sender - subsribed to acceleration, braking, steering, indicators, neutralize commands. create a can massage for the Idan drive by wire system 
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/PoseStamped.h>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>

static const int SYNC_FRAMES = 50;

typedef message_filters::sync_policies::ExactTime<geometry_msgs::TwistStamped, geometry_msgs::PoseStamped>//ApproximateTime
    TwistPoseSync;

class ReducFreq
{
public:
  ReducFreq();
  ~ReducFreq();
private:
ros::Subscriber vel_sub,pose_sub;
ros::Publisher vel_pub,pose_pub;
message_filters::Subscriber<geometry_msgs::TwistStamped> *sub_vel_;
message_filters::Subscriber<geometry_msgs::PoseStamped> *sub_pose_;
message_filters::Synchronizer<TwistPoseSync> *sync_tp_;
int counter = 0;
int drop_num = 3;


  void TwistPoseCallback(const geometry_msgs::TwistStampedConstPtr &twist_msg,
                      const geometry_msgs::PoseStampedConstPtr &pose_msg);

  void poseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);
};

ReducFreq::~ReducFreq()
{
}
ReducFreq::ReducFreq()
{
  ros::NodeHandle n;
  // n.getParam("drop_num",drop_num);
  // ROS_WARN_STREAM("drop_num: "<<drop_num);
  vel_pub = n.advertise<geometry_msgs::TwistStamped>("current_velocity_slow", 10);
  pose_pub = n.advertise<geometry_msgs::PoseStamped>("current_pose_slow", 10);

  sub_pose_ = new message_filters::Subscriber<geometry_msgs::PoseStamped>(n, "current_pose", 50);
  sub_vel_ = new message_filters::Subscriber<geometry_msgs::TwistStamped>(n, "current_velocity", 50);
  sync_tp_ = new message_filters::Synchronizer<TwistPoseSync>(TwistPoseSync(SYNC_FRAMES), *sub_vel_, *sub_pose_);
  sync_tp_->registerCallback(boost::bind(&ReducFreq::TwistPoseCallback, this, _1, _2));

}

void ReducFreq::TwistPoseCallback(const geometry_msgs::TwistStampedConstPtr &twist_msg,
                                      const geometry_msgs::PoseStampedConstPtr &pose_msg) //const
{
  if (counter == 0)
  {
    vel_pub.publish(*twist_msg);
    pose_pub.publish(*pose_msg);
  }
  counter = (counter+1)%drop_num;
}


int main (int argc, char *argv[])

{
  ros::init(argc, argv, "sync_pose_vel");
  ReducFreq SPV;
  ros::spin();
  return 0;
}





