//sender - subsribed to acceleration, braking, steering, indicators, neutralize commands. create a can massage for the Idan drive by wire system 
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <geometry_msgs/Vector3.h>
#include <tf/transform_listener.h>

class RepublishPose
{
public:
  RepublishPose();
  ~RepublishPose();
private:
  void publishTF(geometry_msgs::Pose pose,ros::Time stamp);//
  void poseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);
  ros::Publisher pose_pub_;
  ros::Subscriber local_pose_sub_;
  tf::TransformBroadcaster br_;
};


RepublishPose::RepublishPose()
{
  ros::NodeHandle n;
  local_pose_sub_ = n.subscribe("gnss_local_pose",1000,Callback);
  pose_pub_ = n.advertise<geometry_msgs::PoseStamped>("current_pose", 10);//gnss_pose_unsynced
  ros::spin();
}
RepublishPose::~RepublishPose(){}
 

void RepublishPose::publishTF(geometry_msgs::Pose pose,ros::Time stamp)//
{
  tf::Transform transform;
  double roll, pitch, yaw;
  //transform.setOrigin(tf::Vector3(geo_.y(), geo_.x(), geo_.z()));
  //transform.setOrigin(tf::Vector3(pose.position.y, pose.position.x, pose.position.z));
  transform.setOrigin(tf::Vector3(pose.position.x, pose.position.y, pose.position.z));
  // tf::Quaternion quaternion;
  // quaternion.setRPY(roll_, pitch_, yaw_);
  tf::Quaternion quat_tf;
  quaternionMsgToTF(pose.orientation , quat_tf);
   tf::Matrix3x3(quat_tf).getRPY(roll, pitch, yaw);
  //tf::Matrix3x3(quat_tf).getRPY(roll, yaw , pitch);
  // yaw += 3.141;
  // yaw = -yaw;
  yaw += 3.141/2;
  quat_tf.setRPY(roll, pitch, yaw);
  transform.setRotation(quat_tf);
  br_.sendTransform(tf::StampedTransform(transform, stamp , "map", "gps"));
}




void RepublishPose::poseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  //ROS_WARN_STREAM("I heard:"<< msg->twist.twist);
  //ros::Time current_time = ros::Time::now();


  //set transform - in local gnss frame
  tf::Transform transform;
  transform.setOrigin(tf::Vector3(msg->pose.position.x, msg->pose.position.y, msg->pose.position.z));
  tf::Quaternion quat_tf;
  quaternionMsgToTF(msg->pose.orientation , quat_tf);
  transform.setRotation(quat_tf);


  //convert:
  ros::Time now = ros::Time(0);

  //send converted transform:
  publishTF(msg->pose.pose,current_time );//msg->header.stamp  

  //back to pose msg:
  geometry_msgs::PoseStamped ps;
  ps.header = msg->header;//same time as msg
  ps.header.frame_id = "map";
  pose.pose.position.x = origin.getX();
  pose.pose.position.y = origin.getY();
  pose.pose.position.z = origin.getZ();
  tf::Quaternion Q = transform.getRotation();
  ROS_WARN_STREAM("x: "<<pose.pose.position.x<<" y: "<<pose.pose.position.y<<" z: "<<pose.pose.position.z);
  ROS_WARN_STREAM("rotation: "<<Q.getX()<<"  "<< Q.getY()<<" "<<Q.getZ()<<" "<<Q.getW());
  quaternionTFToMsg(Q , pose.pose.orientation);


  //last_t = send_time;
  //ps.header.stamp = current_time;//ros::Time::now();
  //publish converted transform
  pose_pub_.publish(ts);  
}

int main (int argc, char *argv[])

{
  ros::init(argc, argv, "republish_pose");
  RepublishPose OPV;
  return 0;
}


















//sender - subsribed to acceleration, braking, steering, indicators, neutralize commands. create a can massage for the Idan drive by wire system 
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>

#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
ros::Publisher pub1;
//tf::TransformListener listener;

void Callback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
    tf::StampedTransform transform;
    tf::TransformListener transform_listener;
    ROS_WARN_STREAM("callback");
      try
      {
        ros::Time now = ros::Time(0);//ros::Time::now();
        // transform_listener.waitForTransform("/map", "/world", now, ros::Duration(10.0));
        // transform_listener.lookupTransform("/map", "/world", now, transform);


        // transform_listener.waitForTransform("/gps", "/gnss_frame", now, ros::Duration(10.0));
        // transform_listener.lookupTransform("/gps", "/gnss_frame", now, transform);
        // transform_listener.waitForTransform("/gps", "/map", now, ros::Duration(10.0));
        // transform_listener.lookupTransform("/gps", "/map", now, transform);
        transform_listener.waitForTransform("/map","/gps", now, ros::Duration(10.0));
        ROS_WARN_STREAM("after wait");
        transform_listener.lookupTransform("/map","/gps", now, transform);
        tf::Vector3 origin = transform.getOrigin();
        //ROS_WARN_STREAM("transform: "<<origin.getX()<<"  "<<origin.getY());

        geometry_msgs::PoseStamped pose;
        pose.header.frame_id = "map";
        pose.header.stamp = now;
        pose.pose.position.x = origin.getX();
        pose.pose.position.y = origin.getY();
        pose.pose.position.z = origin.getZ();
        tf::Quaternion Q = transform.getRotation();
        ROS_WARN_STREAM("x: "<<pose.pose.position.x<<" y: "<<pose.pose.position.y<<" z: "<<pose.pose.position.z);
        ROS_WARN_STREAM("rotation: "<<Q.getX()<<"  "<< Q.getY()<<" "<<Q.getZ()<<" "<<Q.getW());
        quaternionTFToMsg(Q , pose.pose.orientation);
        //pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(Q.getX(), Q.getY(), Q.getZ());
        pub1.publish(pose);
      }
      catch (tf::TransformException& ex)
      {
        ROS_ERROR("%s", ex.what());
        ros::Duration(1.0).sleep();
      }
  
}

int main (int argc, char *argv[])

{
  ros::init(argc, argv, "republish_pose");
  ros::NodeHandle n;
  ros::Rate rate(500);
  ros::Subscriber local_pose_sub = n.subscribe("gnss_local_pose",1000,Callback);
  pub1 = n.advertise<geometry_msgs::PoseStamped>("current_pose", 10);//gnss_pose_unsynced
  //ros::spin();

  tf::StampedTransform transform;
  tf::StampedTransform last_transform;
  tf::TransformListener transform_listener;
bool first = true;
  while(n.ok())
  {

      try
      {
        ros::Time now = ros::Time(0);//ros::Time::now();//
        // transform_listener.waitForTransform("/map", "/world", now, ros::Duration(10.0));
        // transform_listener.lookupTransform("/map", "/world", now, transform);


        // transform_listener.waitForTransform("/gps", "/gnss_frame", now, ros::Duration(10.0));
        // transform_listener.lookupTransform("/gps", "/gnss_frame", now, transform);
        // transform_listener.waitForTransform("/gps", "/map", now, ros::Duration(10.0));
        // transform_listener.lookupTransform("/gps", "/map", now, transform);
        transform_listener.waitForTransform("/map","/gps", now, ros::Duration(10.0));

        transform_listener.lookupTransform("/map","/gps", now, transform);
        // transform_listener.waitForTransform("/map","/base_link", now, ros::Duration(10.0));

        // transform_listener.lookupTransform("/map","/base_link", now, transform);
        if(!first)
        {
          if(transform.stamp_ == last_transform.stamp_)
          continue;

          last_transform = transform;
        }
        else
        {
          last_transform = transform;
          first = false;
        }
        
        tf::Vector3 origin = transform.getOrigin();
        //ROS_WARN_STREAM("transform: "<<origin.getX()<<"  "<<origin.getY());
//ROS_WARN_STREAM("now: "<<now);
        geometry_msgs::PoseStamped pose;
        pose.header.frame_id = "map";
        pose.header.stamp = transform.stamp_ ;
        pose.pose.position.x = origin.getX();
        pose.pose.position.y = origin.getY();
        pose.pose.position.z = origin.getZ();
        tf::Quaternion Q = transform.getRotation();
        // ROS_WARN_STREAM("x: "<<pose.pose.position.x<<" y: "<<pose.pose.position.y<<" z: "<<pose.pose.position.z);
        // ROS_WARN_STREAM("rotation: "<<Q.getX()<<"  "<< Q.getY()<<" "<<Q.getZ()<<" "<<Q.getW());
        quaternionTFToMsg(Q , pose.pose.orientation);
        //pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(Q.getX(), Q.getY(), Q.getZ());
        pub1.publish(pose);
      }
      catch (tf::TransformException& ex)
      {
        ROS_ERROR("%s", ex.what());
        ros::Duration(1.0).sleep();
        continue;
      }



  //ros::spin();
  rate.sleep();

  }
  return 0;
}





