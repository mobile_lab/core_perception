//sender - subsribed to acceleration, braking, steering, indicators, neutralize commands. create a can massage for the Idan drive by wire system 
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
ros::Publisher pub1;
//tf::TransformListener listener;

void Callback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
    tf::StampedTransform transform;
    tf::TransformListener transform_listener;
    ROS_WARN_STREAM("callback");
      try
      {
        ros::Time now = ros::Time(0);//ros::Time::now();
        // transform_listener.waitForTransform("/map", "/world", now, ros::Duration(10.0));
        // transform_listener.lookupTransform("/map", "/world", now, transform);


        // transform_listener.waitForTransform("/gps", "/gnss_frame", now, ros::Duration(10.0));
        // transform_listener.lookupTransform("/gps", "/gnss_frame", now, transform);
        // transform_listener.waitForTransform("/gps", "/map", now, ros::Duration(10.0));
        // transform_listener.lookupTransform("/gps", "/map", now, transform);
        transform_listener.waitForTransform("/map","/gps", now, ros::Duration(10.0));
        ROS_WARN_STREAM("after wait");
        transform_listener.lookupTransform("/map","/gps", now, transform);
        tf::Vector3 origin = transform.getOrigin();
        //ROS_WARN_STREAM("transform: "<<origin.getX()<<"  "<<origin.getY());

        geometry_msgs::PoseStamped pose;
        pose.header.frame_id = "map";
        pose.header.stamp = now;
        pose.pose.position.x = origin.getX();
        pose.pose.position.y = origin.getY();
        pose.pose.position.z = origin.getZ();
        tf::Quaternion Q = transform.getRotation();
        ROS_WARN_STREAM("x: "<<pose.pose.position.x<<" y: "<<pose.pose.position.y<<" z: "<<pose.pose.position.z);
        ROS_WARN_STREAM("rotation: "<<Q.getX()<<"  "<< Q.getY()<<" "<<Q.getZ()<<" "<<Q.getW());
        quaternionTFToMsg(Q , pose.pose.orientation);
        //pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(Q.getX(), Q.getY(), Q.getZ());
        pub1.publish(pose);
      }
      catch (tf::TransformException& ex)
      {
        ROS_ERROR("%s", ex.what());
        ros::Duration(1.0).sleep();
      }
  
}

int main (int argc, char *argv[])

{
  ros::init(argc, argv, "republish_pose");
  ros::NodeHandle n;
  ros::Rate rate(500);
  //ros::Subscriber odom_sub = n.subscribe("gnss_local_pose",1000,Callback);
  pub1 = n.advertise<geometry_msgs::PoseStamped>("current_pose", 10);//gnss_pose_unsynced
  //ros::spin();

  tf::StampedTransform transform;
  tf::StampedTransform last_transform;
  tf::TransformListener transform_listener;
bool first = true;
  while(n.ok())
  {

      try
      {
        ros::Time now = ros::Time(0);//ros::Time::now();//
        // transform_listener.waitForTransform("/map", "/world", now, ros::Duration(10.0));
        // transform_listener.lookupTransform("/map", "/world", now, transform);


        // transform_listener.waitForTransform("/gps", "/gnss_frame", now, ros::Duration(10.0));
        // transform_listener.lookupTransform("/gps", "/gnss_frame", now, transform);
        // transform_listener.waitForTransform("/gps", "/map", now, ros::Duration(10.0));
        // transform_listener.lookupTransform("/gps", "/map", now, transform);
        transform_listener.waitForTransform("/map","/gps", now, ros::Duration(10.0));

        transform_listener.lookupTransform("/map","/gps", now, transform);
        // transform_listener.waitForTransform("/map","/base_link", now, ros::Duration(10.0));

        // transform_listener.lookupTransform("/map","/base_link", now, transform);
        if(!first)
        {
          if(transform.stamp_ == last_transform.stamp_)
          continue;

          last_transform = transform;
        }
        else
        {
          last_transform = transform;
          first = false;
        }
        
        tf::Vector3 origin = transform.getOrigin();
        //ROS_WARN_STREAM("transform: "<<origin.getX()<<"  "<<origin.getY());
//ROS_WARN_STREAM("now: "<<now);
        geometry_msgs::PoseStamped pose;
        pose.header.frame_id = "map";
        pose.header.stamp = transform.stamp_ ;
        pose.pose.position.x = origin.getX();
        pose.pose.position.y = origin.getY();
        pose.pose.position.z = origin.getZ();
        tf::Quaternion Q = transform.getRotation();
        // ROS_WARN_STREAM("x: "<<pose.pose.position.x<<" y: "<<pose.pose.position.y<<" z: "<<pose.pose.position.z);
        // ROS_WARN_STREAM("rotation: "<<Q.getX()<<"  "<< Q.getY()<<" "<<Q.getZ()<<" "<<Q.getW());
        quaternionTFToMsg(Q , pose.pose.orientation);
        //pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(Q.getX(), Q.getY(), Q.getZ());
        pub1.publish(pose);
      }
      catch (tf::TransformException& ex)
      {
        ROS_ERROR("%s", ex.what());
        ros::Duration(1.0).sleep();
        continue;
      }



  //ros::spin();
  rate.sleep();

  }
  return 0;
}





